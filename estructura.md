Exemples de com aconseguir ISBN, títol i autor:
```
DELETE /pompeia
# Crear la llibreria
PUT /pompeia
{
  "mappings": {
    "properties": {
      "ISBN" : {"type": "keyword"},
      "titol": {"type": "text"},
      "author": {"type": "text"}
    }
  }
}

GET /pompeia/_mapping

# https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-index_.html
POST /pompeia/_doc/978-84-664-2882-8
{
  "ISBN" : "978-84-664-2882-8",
  "titol" : "BENVOLGUDA",
  "author": "Empar Moliner"
}


GET /pompeia/_doc/978-84-664-2882-8
```
